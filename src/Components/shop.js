import { Component } from "react";

import { connect } from "react-redux";
import { buyLaptop, buyMobile,fetchUsers } from "./redux/actions";

import "./shop.css"

class Shop extends Component {
    // state = {
    //     numberOfLaptops:100
    // }

    // buyLaptop =()=>{
    //     this.setState({numberOfLaptops: this.state.numberOfLaptops -1})
    // }
    render() {
        return (
            <div >
                <h1 className="title">Welcome to VShop</h1>
                <div className="items">
                    <div className="item">
                        <p>Dell Inspiron Laptop</p>
                        <p>Avilable: {this.props.numOfLaptops}</p>
                        <button onClick={this.props.buyLaptop}>BUY</button>
                    </div>
                    <div className="item">
                        <p>Samsung a50s</p>
                        <p>Avilable: {this.props.numOfMobiles}</p>
                        <button onClick={this.props.buyMobile}>BUY</button>
                    </div>
                    <div className="item">
                        <p>Get users count</p>
                        <p>Count: {this.props.users.length}</p>
                        <button onClick={this.props.fetchUsers}>BUY</button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateTOProps = (state) => {
    console.log(state)
    return {
        numOfLaptops: state.laptop.numOfLaptops,
        numOfMobiles: state.mobile.numOfMobiles,
        users:state.users.users
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        buyLaptop: () => dispatch(buyLaptop()),
        buyMobile: () => dispatch(buyMobile()),
        fetchUsers: () => dispatch(fetchUsers())
    }
}

export default connect(mapStateTOProps, mapDispatchToProps)(Shop)


