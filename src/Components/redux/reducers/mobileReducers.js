import { BUY_Mobile } from "../actions/actionsTypes";

const initialState = {
    numOfMobiles: 1000
}

const mobileReducer = (state = initialState, action) => {
    switch (action.type) {
        case BUY_Mobile:
            return { numOfMobiles: state.numOfMobiles - 1 }
        default:
            return state    
    }
}

export default mobileReducer