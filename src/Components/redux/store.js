import {legacy_createStore as createstore, combineReducers,applyMiddleware} from "redux"
import thunk from "redux-thunk" 

import logger from "redux-logger"

import laptopReducer from "./reducers/laptopReducer"
import mobileReducer from "./reducers/mobileReducers"
import userReducer from "./reducers/usersReducer"

const combineStore = combineReducers({
    laptop:laptopReducer,
    mobile:mobileReducer,
    users:userReducer
})
const store = createstore(combineStore,applyMiddleware(thunk,logger))


export default store

